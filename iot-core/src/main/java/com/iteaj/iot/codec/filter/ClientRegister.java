package com.iteaj.iot.codec.filter;

import com.iteaj.iot.FrameworkComponent;
import com.iteaj.iot.Message;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.util.AttributeKey;

/**
 * 客户端编号注册和连接过滤
 */
public interface ClientRegister<C extends FrameworkComponent> extends Filter<C> {

    /**
     * 此连接是否可激活
     * @see ChannelInboundHandler#channelActive(ChannelHandlerContext)
     * @param channel
     * @param component
     * @return true：允许客户端连接  false：直接关闭连接
     */
    default boolean isActivation(Channel channel, C component) {
        return true;
    }

    /**
     * 客户端编号注册
     * 主要用于将连接和客户端编号进行绑定
     * @see Channel#attr(AttributeKey)
     * @param head {@link com.iteaj.iot.SocketMessage#doBuild(byte[])}
     * @param equipCode 设编号 在没有注册之前此参数是null
     * @param channel 当前连接通道
     * @param component 组件
     * @return @param head
     */
    default Message.MessageHead register(Message.MessageHead head
            , String equipCode, Channel channel, C component) {
        return head;
    }

}
