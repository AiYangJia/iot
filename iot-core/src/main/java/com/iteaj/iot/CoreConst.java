package com.iteaj.iot;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.websocket.HttpRequestWrapper;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.util.AttributeKey;

public interface CoreConst {

    /**
     * 客户端解码器
     */
    String CLIENT_DECODER_HANDLER = "ClientProtocolDecoder";

    /**
     * 客户端编码器
     */
    String CLIENT_ENCODER_HANDLER = "ClientProtocolEncoder";

    /**
     * 客户端业务处理器
     */
    String CLIENT_SERVICE_HANDLER = "ClientServiceHandler";

    /**
     * 服务端解码器
     */
    String SERVER_DECODER_HANDLER = "ServerProtocolDecoder";

    /**
     * 服务端编码器
     */
    String SERVER_ENCODER_HANDLER = "ServerProtocolEncoder";

    /**
     * 服务端业务处理器
     */
    String SERVER_SERVICE_HANDLER = "ServerServiceHandler";

    /**
     * 存活状态事件处理器
     */
    String IDLE_STATE_EVENT_HANDLER = "IdleStateEventHandler";

    /**
     * 客户端上下线、存活等事件处理器
     */
    String EVENT_MANAGER_HANDLER = "EventManagerHandler";

    /**
     * 设备编号的KEY
     */
    AttributeKey EQUIP_CODE = AttributeKey.newInstance("IOT:EquipCode");

    /**
     * Websocket请求对象
     */
    AttributeKey<HttpRequestWrapper> WEBSOCKET_REQ = AttributeKey.newInstance("IOT:WEBSOCKET:REQ");

    /**
     * Websocket关闭标识
     */
    AttributeKey<Boolean> WEBSOCKET_CLOSE = AttributeKey.newInstance("IOT:WEBSOCKET:CLOSE");

    /**
     * 客户端key
     */
    AttributeKey<ClientConnectProperties> CLIENT_KEY = AttributeKey.newInstance("IOT:ClientKey");
}
