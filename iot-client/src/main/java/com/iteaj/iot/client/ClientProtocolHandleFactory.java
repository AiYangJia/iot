package com.iteaj.iot.client;

import com.iteaj.iot.Protocol;
import com.iteaj.iot.business.BusinessFactory;

public class ClientProtocolHandleFactory extends BusinessFactory<ClientProtocolHandle> {

    @Override
    protected Class<? extends Protocol> getProtocolClass(ClientProtocolHandle item) {
        return item.protocolClass();
    }

    @Override
    protected Class<ClientProtocolHandle> getServiceClass() {
        return ClientProtocolHandle.class;
    }
}
