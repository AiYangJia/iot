package com.iteaj.iot.test.filter;

import com.iteaj.iot.CoreConst;
import com.iteaj.iot.Message;
import com.iteaj.iot.codec.filter.CombinedFilter;
import com.iteaj.iot.server.websocket.impl.DefaultWebSocketServerComponent;
import com.iteaj.iot.websocket.HttpRequestWrapper;
import com.iteaj.iot.websocket.WebSocketFilter;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.util.Attribute;
import org.springframework.stereotype.Component;

@Component
public class TestWebSocketFilter implements WebSocketFilter<DefaultWebSocketServerComponent> {

    @Override
    public boolean isActivation(Channel channel, DefaultWebSocketServerComponent component) {
        return WebSocketFilter.super.isActivation(channel, component);
    }

    @Override
    public Message.MessageHead register(Message.MessageHead head, String equipCode
            , Channel channel, DefaultWebSocketServerComponent component) {
        if(equipCode == null) {
            Attribute<HttpRequestWrapper> attr = channel.attr(CoreConst.WEBSOCKET_REQ);
            HttpRequestWrapper httpRequestWrapper = attr.get();
            httpRequestWrapper.getQueryParam("client").ifPresent(deviceSn -> head.setEquipCode(deviceSn));
        } else {
            head.setEquipCode(equipCode);
        }

        return head;
    }

    @Override
    public HttpResponseStatus authentication(HttpRequest request) {
        return HttpResponseStatus.OK;
    }
}
