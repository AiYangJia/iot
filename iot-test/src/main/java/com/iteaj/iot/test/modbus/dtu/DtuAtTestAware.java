package com.iteaj.iot.test.modbus.dtu;

import com.iteaj.iot.server.dtu.DefaultDtuMessageAware;
import io.netty.buffer.ByteBuf;

public class DtuAtTestAware extends DefaultDtuMessageAware {

    public DtuAtTestAware() {
        super(null);
    }

    @Override
    public boolean isAt(String equipCode, byte[] message, ByteBuf msg) {
        return new String(message).startsWith(equipCode);
    }
}
