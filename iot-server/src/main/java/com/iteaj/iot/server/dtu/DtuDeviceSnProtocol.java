package com.iteaj.iot.server.dtu;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DtuDeviceSnProtocol extends ClientInitiativeProtocol<ServerMessage> {

    private static Logger logger = LoggerFactory.getLogger(DtuDeviceSnProtocol.class);

    public DtuDeviceSnProtocol(ServerMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected ServerMessage doBuildResponseMessage() {
        return null;
    }

    @Override
    protected void doBuildRequestMessage(ServerMessage requestMessage) {
        if(logger.isTraceEnabled()) {
            logger.trace("获取DTU设备协议");
        }
    }

    @Override
    public ProtocolType protocolType() {
        return DtuCommonProtocolType.DEVICE_SN;
    }
}
