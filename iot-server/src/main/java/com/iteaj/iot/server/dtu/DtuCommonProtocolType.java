package com.iteaj.iot.server.dtu;

import com.iteaj.iot.ProtocolType;

public enum DtuCommonProtocolType implements ProtocolType {

    AT("at指令"),
    HEARTBEAT("心跳"),
    DEVICE_SN("DTU设备编号")
    ;

    private String desc;

    DtuCommonProtocolType(String desc) {
        this.desc = desc;
    }

    @Override
    public Enum getType() {
        return this;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
