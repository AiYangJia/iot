package com.iteaj.iot.client;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.proxy.ProxyServerMessage;
import com.iteaj.iot.server.protocol.ServerInitiativeProtocol;

import java.io.IOException;

public class ClientProxyProtocol extends ServerInitiativeProtocol<ProxyServerMessage> {

    @Override
    protected ProxyServerMessage doBuildRequestMessage() throws IOException {
        return null;
    }

    @Override
    protected void doBuildResponseMessage(ProxyServerMessage message) {

    }

    @Override
    public ProtocolType protocolType() {
        return null;
    }
}
